<?php

// lerenWerkenMetEcho();
// verschilTussenByValueEnByReference();
// lerenWerkenMetConstanten();
// echo bizarrerieRekenkundigeOperator();
variableVariable();

function verschilTussenByValueEnByReference() {
    $voornaam = 'Anna';
    $familienaam = 'Thomas';
    $adresVoornaam = &$voornaam;
    echo "De voornaam is $voornaam <br />";
    echo "Het adres van de voornaam is $adresVoornaam <br />";
    $voornaam = 'Jan';
    echo "De voornaam is $voornaam <br />";
    echo "Het adres van de voornaam is $adresVoornaam <br />";
    $adresVoornaam = 'Hannah';
    echo "De voornaam is $voornaam <br />";
    echo "Het adres van de voornaam is $adresVoornaam <br />";
}

function lerenWerkenMetEcho() {
    /*
    * Echo
    * Enkelvoudige tekst
    * String interpolatie
    */
    echo 'Leren programmeren met PHP';
    echo '<br />';
    echo 'Eerste les';
    echo '<br />';
    
    $voornaam = 'Mohamed';
    $familienaam = 'El Farisi';
    echo "Je volledige naam is: $voornaam $familienaam";
    // nu met html en css
    echo "<p>Je volledige naam is: <span style=\"color: red;\">$voornaam</span> <span style=\"color: green;\"> $familienaam</span></p>";
}

function lerenWerkenMetConstanten() {
    define('PI', 3.14);
    define('MAX', 20);
    define('MIN', 2);
    // constanten niet gebruiken in string interpolatie
    // dus wel concatenatie
    echo 'De waarde van PI is ' . constant('PI');
    $value = 100;
    if ($value >= constant('MIN') && $value <= constant('MAX')) {
        echo 'bingo!';
    } else {
        echo 'De ingevoerde waarde ligt niet tussen ' . 
            constant('MIN') . ' en ' . constant('MAX') . '.';
    }
}


function bizarrerieRekenkundigeOperator() {
    $int = 10;
    // return $int++;
    return ++$int;
}

function variableVariable() {
    $watWilJeTonen = 'voornaam';
    $voornaam = 'Mo';
    $familienaam = 'Inghelbrecht';
    $watWilJeTonen = 'voornaam';
    echo $$watWilJeTonen;
    $watWilJeTonen = 'familienaam';
    echo $$watWilJeTonen;
}
// https://ubuntuforums.org/showthread.php?t=525257