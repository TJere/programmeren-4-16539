<?php

$movies[0] = 'De witte';
$movies[1] = 'Game of thrones';
$movies[2] = 'Hercule Poirot';
$movies[3] = 'Upstairs Downstairs';

$films = array(
    0 => 'De witte',
    1 => 'Game of thrones',
    2 => 'Hercule Poirot',
    3 => 'Upstairs Downstairs'
    );
    
echo "Mijn lievelingsfilm is {$movies[2]}<br />";
echo "{$films[3]} is een film van het jaar stilletjes...<br />";

$movies[10] = 'Game over';
echo "Mijn lievelingsfilm is {$movies[10]}<br />";
echo "Mijn lievelingsfilm is {$movies[8]}<br />";

$adres = array(
    'Voornaam' => 'Mo',
    'Familienaam' => 'Jansens',
    'Straat' => 'Pompoenstraan 20',
    'Stad' => 'Antwerpen',
    'Postcode' => '2000'
);

echo "{$adres['Voornaam']} {$adres['Familienaam']} woont in de {$adres['Straat']} in {$adres['Postcode']} in {$adres['Stad']}.";
