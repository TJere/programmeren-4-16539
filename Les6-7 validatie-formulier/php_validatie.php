<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>
    <?php
    $firstnameErr = $lastnameErr = $emailErr = $adressErr = $cityErr = $postalcodeErr = "";
    $firstname = $lastname = $email = $adress = $city = $postalcode = "";
    $hasError = false;
    
     
     
    if ($_SERVER["REQUEST_METHOD"] == "POST"){
     $firstname=$_POST["firstname"];
     $lastname=$_POST["lastname"];
     $email=$_POST["email"];
     $adress=$_POST["address"];
     $city=$_POST["city"];
     $postalcode=$_POST["postalcode"];
        
        if(empty($firstname)){
            $firstnameErr = "Voornaam is verplicht";
        }
       
        if(empty($lastname)){
            $lastnameErr = "Achternaam is verplicht";
        }
        
        if(empty($email)){
            $emailErr = "email is verplicht";
        }else{
            $email = test_input($email);
            if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
                $emailErr = "Geef een geldig mailadres";
            }
        }
        
        if(!empty($firstnameErr) || !empty($lastnameErr) || !empty($emailErr)){
            $hasError = true;
        }
        
    }
    function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
    ?>
    <div>
        <form method="Post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
             <!--<form method="Post" action="<?php echo ($hasError? htmlspecialchars($_SERVER["PHP_SELF"]):  "welkom.php");?>">-->
            <fieldset>
                <legend>Accountgegevens</legend>
                <div>
                    <label for="firstname">Voornaam:<span> * </span></label>
                    <input id="firstname" type="text" name="firstname" value="<?php echo $firstname ?>" >
                    <span class="error"> <?php echo $firstnameErr;?></span>
                </div>
                <div>
                    <label for="lastname">Familienaam:<span> * </span></label>
                    <input id="lastname" type="text" name="lastname" value="<?php echo $lastname ?>">
                    <span class="error"> <?php echo $lastnameErr;?></span>
                </div>
                <div>
                    <label for="email">E-mail adres:<span> * </span></label>
                    <input id="email" type="text" name="email" value="<?php echo $email ?>">
                    <span class="error"> <?php echo $emailErr;?></span>
                </div>
                <div>
                    <label for="password1">Paswoord:<span> * </span></label>
                    <input id="password1" type="password" name="password1">
                </div>
               
            </fieldset>
            <fieldset>
                <legend>Adresgegevens</legend>
                <div>
                    <label for="address">Adres<span> * </span></label>
                    <input id="address" type="text" name="address" value="<?php echo $adress ?>">
                </div>
                
                <div>
                    <label for="city">Stad:<span> * </span></label>
                    <input id="city" type="text" name="city" value="<?php echo $city ?>" >
                </div>
                <div>
                    <label for="postalcode">Postcode:<span> * </span></label>
                    <input id="postalcode" type="text" name="postalcode" value="<?php echo $postalcode ?>">
                </div>
                
            </fieldset>
                
            <div><input type="submit" name="submit" value="Verzenden" /></div>
        </form>
    </div>
    <div>
        
 <?php
 if ($_SERVER["REQUEST_METHOD"] == "POST" && !$hasError){
 ?>
<h1>Welkom</h1>
<ul>
<li>Voornaam: <?php echo $_POST['firstname'] ?></li>
<li>Familienaam: <?php echo $_POST['lastname'] ?></li>
<li>Email: <?php echo $_POST['email'] ?></li>
<li>Adres: <?php echo $_POST['address'] ?></li>
<li>Postcode: <?php echo $_POST['postalcode'] ?></li>
<li>Stad: <?php echo $_POST['city'] ?></li>
</ul>
    </div>
    
    <?php
 }
    ?>
</body>

</html>
